		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			
			<div id="sidebar" class="sidebar responsive">
			
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<ul class="nav nav-list" id="MenuLateral">

					<li class="hsub" id="inicioprincipal">
						<a href="<?php echo base_url();?>principal">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> MENU PRINCIPAL </span>
						</a>
						<b class="arrow"></b>
					</li>

					<li class="hsub" id="menu-empleado">
						<a href="<?php echo base_url();?>views/empleado" id="ModuloEmpleado">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> EMPLEADO </span>

						</a>

						<b class="arrow"></b>
					</li>
					
				</ul>
				
			</div>
		</div>