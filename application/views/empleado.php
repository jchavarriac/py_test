<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">try{ace.settings.check('navbar' , 'fixed')}catch(e){}</script>
		<ul class="breadcrumb">
			<li> <i class="icon-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li class="active">Empleado</li>
		</ul>
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1> <i class="icon-hand-right icon-animated-hand-pointer blue"></i>
				Mantenedor Empleado
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="row">
					<div class="col-xs-12">
						<div class="widget-box">
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" role="form" id="frm-registro" name="frm-registro" action-1="<?php echo base_url();?>empleado/registrar" action-2="<?php echo base_url();?>empleado/actualizar">
										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-6">
													<div class="form-group">
														<label class="col-sm-4 control-label no-padding-right" for="codigo-empleado">Código</label>
														<div class="col-sm-8">
															<input type="text" class="form-control hidden" id="id-empleado" name="id-empleado">
															<input class="form-control validate[required,minSize[8],maxSize[8]" type="text" id="keycode-empleado" name="keycode-empleado" placeholder=" 00000000 "  readonly/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label no-padding-right" for="dni-empleado">DNI</label>
														<div class="col-sm-8">
															<input class="form-control validate[required,minSize[8],maxSize[8]" type="text" id="dni-empleado" name="dni-empleado" placeholder="DNI" maxlength="8" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label no-padding-right" for="nombre-empleado">Nombre</label>
														<div class="col-sm-8">
															<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="nombre-empleado" name="nombre-empleado" placeholder=" Nombre " style="text-transform: uppercase;"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label no-padding-right" for="apellidos-empleado">Apellido</label>
														<div class="col-sm-8">
															<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="apellidos-empleado" name="apellidos-empleado" placeholder=" Apellido " style="text-transform: uppercase;"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-4 control-label no-padding-right" for="fechanac-empleado">Fecha de Nacimiento</label>
														<div class="col-lg-8">
															<div class="input-group">
																<input type="text" id="fechanac-empleado" name="fechanac-empleado" class="form-control date-picker-edad validate[required,custom[date]]" placeholder="Fecha de Nacimiento" class="col-xs-10 col-sm-5" />
																<span class="input-group-addon">
																	<i class="glyphicon glyphicon-calendar bigger-110"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6">
													<div class="form-group">
														<label class="control-label col-lg-3 no-padding-right"for="planta-empleado">Planta</label>
														<div class="col-lg-8">
															<select id="planta-empleado" name="planta-empleado" class="form-control validate[required] SelectAjax" data-source="<?php echo base_url();?>configuracion/servicios/get_constante_byclase/4" attrval="int_valor" attrdesc="var_descripcion">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-lg-3 no-padding-right"for="area-empleado">Area</label>
														<div class="col-lg-8">
															<select id="area-empleado" name="area-empleado" class="form-control validate[required] SelectAjax" data-source="<?php echo base_url();?>configuracion/servicios/get_constante_byclase/4" attrval="int_valor" attrdesc="var_descripcion">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-lg-3 no-padding-right"for="cargo-empleado">Cargo</label>
														<div class="col-lg-8">
															<select id="cargo-empleado" name="cargo-empleado" class="form-control validate[required] SelectAjax" data-source="<?php echo base_url();?>configuracion/servicios/get_constante_byclase/4" attrval="int_valor" attrdesc="var_descripcion">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label no-padding-right" for="anexo">Anexo</label>
														<div class="col-lg-8">
															<input type="text" class="form-control validate[required,minSize[6],maxSize[15]]" id="anexo-empleado" name="anexo-empleado" placeholder="Anexo" maxlength="4">
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-3 control-label no-padding-right" for="email-empleado">Email</label>
														<div class="col-lg-8">
															<input type="email" class="form-control validate[required,minSize[6],maxSize[15]]" id="email-empleado" name="email-empleado" placeholder="Email">
														</div>
													</div>
												</div>														
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-4 col-md-8">
												<button id="btn-cancelar-empleado" class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>Cancelar
												</button>
												<button id="btn-guardar-empleado" class="btn btn-primary" type="button">
													<i class="glyphicon glyphicon-save bigger-110"></i>Guardar
												</button>
												<button id="btn-actualizar-empleado" class="btn btn-primary" type="button">
													<i class="glyphicon glyphicon-save bigger-110"></i>Actualizar
												</button>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="col-xs-12">
											<div class="box-header">
												<h3 class="header smaller lighter blue">Lista de Empleado</h3>
											</div>
											<div class="box-body table-responsive">
												<table class="table table-striped table-bordered bootstrap-datatable datatable" 
												id="empleado_table" data-source ="<?php echo base_url();?>
													servicios/get_empleado_all">
													<thead>
														<tr>
															<th>Código</th>
															<th>DNI</th>
															<th>Nombre</th>
															<th>Apellido</th>
															<th>Cargo</th>
															<th>Fec. Nacimiento</th>
															<th>Planta</th>
															<th>Area</th>
															<th>Anexo</th>
														</tr>
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--MODALS-->
			<div class="modal fade" id="modal-actualizar-empleado" >
				<div class="modal-dialog" style="width:800px;">
					<div class="modal-content">
						<div class="modal-header">
							<h3>Actualizar Célula</h3>
						</div>					
						<form role="form" id="frm-empleado2" name="frm-empleado2" action-1="<?php echo base_url();?>
							configuracion/red/actualizar_empleado" class="form-horizontal">
							<div class="modal-body">
								<fieldset>							
									<div class="form-group">
										<input id="empleado-id" name="empleado-id" class="hide">
										<label class="col-sm-3 control-label no-padding-right" for="nombre-empleado2">Nombre</label>
										<div class="col-sm-8">
											<input type="text" id="nombre-empleado2" name="nombre-empleado2" placeholder=" Nombre de Célula " class="form-control validate[required,minSize[2],maxSize[30]"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 no-padding-right" for="responsable-empleado2">Responsable</label>
										<div class="col-sm-8" style="width: 505px">
											<select class="form-control chosen-select" id="responsable-empleado2" name="responsable-empleado2" attrval="persona_id" attrdesc="persona" class="validate[required]"></select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3 no-padding-right" for="red-empleado2">Red</label>
										<div class="col-sm-8">
											<select id="red-empleado2" name="red-empleado2" class="form-control validate[required] SelectAjax" data-source="<?php echo base_url();?>
												configuracion/servicios/get_red_bynopastoral" attrval="red_id" attrdesc="red">
											</select>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="modal-footer">
								<a href="#" class="btn" data-dismiss="modal">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									Cancelar
								</a>
								<button id="btn-actualizar-empleado" type="button" class="btn btn-warning">
									<i class="glyphicon glyphicon-edit bigger-110"></i>
									Actualizar
								</button>
							</div>
							
						</form>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>