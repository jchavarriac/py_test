<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class views extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function empleado()
	{		
		$dataheader['title'] = 'Empleado';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('empleado');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/empleado.js';
		$datafooter['active'] = 'menu-empleado';
		$datafooter['dropactive'] = 'submenu-red';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

	public function reporte()
	{		
		$dataheader['title'] = 'Servicios';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('reporte');		
		$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/reporte.js';
		$datafooter['active'] = 'menu-reporte';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);		
	}

}