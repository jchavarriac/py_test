<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class principal extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$dataheader['title'] = 'Home Principal - ';
		$this->load->view('templates/headers.php',$dataheader);		
		$this->load->view('templates/menu.php');
		$this->load->view('principal');
		$datafooter['jsvista'] = '';
		$datafooter['active'] = 'inicioprincipal';
		$datafooter['dropactive'] = '';
		$datafooter['subactive'] = '';
		$this->load->view('templates/footer.php',$datafooter);
	}
}