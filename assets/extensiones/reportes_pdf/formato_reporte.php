<?php

$title = $_POST['title'];
$detalle = $_POST['data'];
$detalle = json_decode($detalle);

$url = 'http://'.$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT']."/py_incidencias/assets/images/logo_dinet.jpg";

ob_start();
?>
    
    <style type="text/css">
	<!--
	#header{
		width: 58%;
	}
	#detalle{
		margin-top: 0px;
		width: 100%;
	}
	#footer{
		width: 70%;
	}

	</style>
	
	<page backtop="1mm" backbottom="9mm" backleft="2mm" backright="2mm">
		<page_footer>
			<div>
				<hr/>
				<span> Página [[page_cu]]/[[page_nb]]</span>
			</div>
	    </page_footer>
		<table id="detalle">
			<tr>
				<td colspan="5">
					<img src="<?php echo $url; ?>" style="text-align:center;float:right;width: 30%;"/>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<h1 style="margin-bottom: 0;">LISTA DE INCIDENCIAS</h1>
					<?php if($title!=""):?>
						<h3><?php echo $title; ?></h3>
					<?php endif?>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<div style="background-color:#5858FA;height:2px;"></div>
				</td>
			</tr>
			<tr style="background-color:#F5F5F5;">
                <td style="text-align: left;width:10%;"><b>Código</b></td>
                <td style="text-align: left;width:40%;"><b>Titulo</b></td>
                <td style="text-align: center;width:20%;"><b>Usuario</b></td>
                <td style="text-align: center;width:20%;"><b>Fec. Incidencia</b></td>
                <td style="text-align: center;width:10%;"><b>Estado</b></td>
            </tr>
            <tr>
				<td colspan="5">
					<div style="background-color:#5858FA;height:2px;"></div>
				</td>
			</tr>
			<?php
        		if (count($detalle)==1){
        			$date = new DateTime($detalle[0]->fecharegistro);
        			echo '<tr >
		                    <td style="width:10%;" >'.$detalle[0]->keycode.'</td>
		                    <td style="width:40%;">'.$detalle[0]->titulo.'</td>
		                    <td style="width:20%;text-align: center;">'.$detalle[0]->usuario_creo.'</td>
		                    <td style="width:20%;text-align: center;">'.$date->format('Y-m-d').'</td>
		                    <td style="width:10%;text-align: right;" >'.$detalle[0]->estado.'</td>
		                </tr>';
        		}else{
        			foreach ($detalle as $key => $row) {
        				$date = new DateTime($row->fecharegistro);
        				echo '<tr >
		                    <td style="width:10%;" >'.$row->keycode.'</td>
		                    <td style="width:40%;">'.$row->titulo.'</td>
		                    <td style="width:20%;text-align: center;">'.$row->usuario_creo.'</td>
		                    <td style="width:20%;text-align: center;">'.$date->format('Y-m-d').'</td>
		                    <td style="width:10%;text-align: center;">'.$row->estado.'</td>
		                </tr>';
        			}	
        		}
        	?>
			<tr>
				<td colspan="5">
					<div style="background-color:#5858FA;height:2px;"></div>
				</td>
			</tr>
		</table>
    </page>

<?php 
  	
  	$content = ob_get_clean();

    require_once(dirname(__FILE__).'/../html2pdf/html2pdf.class.php');
    $html2pdf = new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf->Output('reporte_'.date("d-m-Y").'.pdf');

?>