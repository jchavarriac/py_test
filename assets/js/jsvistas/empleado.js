var empleadoTables;
var id_planta_area=0;
$(document).ready(function(){

	$("#btn-actualizar-empleado").hide();

	$('#dni-empleado').numeric();
	$('#anexo-empleado').numeric();

	var successEdit = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Actualización correcta", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}
		});
		id_planta_area = 0;
		$("#frm-registro").reset();
		$("#btn-guardar-empleado").show();
		$("#btn-actualizar-empleado").hide();
		reloadTable();
	}

	var errorEdit = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Actualizar información", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });
	}

	var successEmpleado = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Registro correcto", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}			
		});	
		reloadTable();
		$("#frm-registro").reset();
	}

	var errorEmpleado = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Registrar", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });	
	}

	$('#btn-guardar-empleado').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-1"),{formulario:$("#frm-registro").serializeObject()}, successEmpleado, errorEmpleado);
			}
		});
	});

	$("#btn-actualizar-empleado").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-2"),{formulario:$("#frm-registro").serializeObject()}, successEdit, errorEdit);
			}
		});
	});

	$("#btn-cancelar-empleado").click(function(event){
		event.preventDefault();	
		id_planta_area = 0;
		$("#frm-registro").reset();
	});

	$("#planta-empleado").change(function(event){
		event.preventDefault();	
		cargar_combo_area($("#planta-empleado").val());
	});

	var empleadoTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					$("#id-empleado").val(aData.idempleado);
					$("#keycode-empleado").val(aData.keycode);
					$("#dni-empleado").val(aData.dni);
					$("#nombre-empleado").val(aData.nombre);
					$("#apellidos-empleado").val(aData.apellidos);
					$("#planta-empleado").val(aData.idplanta);
					cargar_combo_area(aData.idplanta);
					id_planta_area = aData.idarea;
					$("#cargo-empleado").val(aData.idcargo);
					if(aData.fechanacimiento!=""){
						var from = aData.fechanacimiento.substring(0,10);
						var from = from.split("-");
						$("#fechanac-empleado").val(from[2]+"/"+from[1]+"/"+from[0]);
					}
					$("#email-empleado").val(aData.correo);
					$("#btn-guardar-empleado").hide();
					$("#btn-actualizar-empleado").show();
				},
			},
			]
	});

	var EmpleadoOptions = {
		"aoColumns":[
			{ "mDataProp": "keycode"},
			{ "mDataProp": "dni"},
			{ "mDataProp": "nombre"},
			{ "mDataProp": "apellidos"},
			{ "mDataProp": "nom_cargo"},
			{ "mDataProp": "fechanacimiento"},
			{ "mDataProp": "nom_planta"},
			{ "mDataProp": "nom_area"},
			{ "mDataProp": "anexo"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": empleadoTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	empleadoTables = createDataTable2('empleado_table',EmpleadoOptions);
	reloadTable();

});

function reloadTable(){
	empleadoTables.fnReloadAjax(base_url+"servicios/get_empleado_all/");
	cargar_combo_planta();
	cargar_combo_cargo();
	obtenerkeyCode();
}

function cargar_combo_planta(){
	 $('#planta-empleado').text("");
	 $.ajax({
        type: "GET",
        url: base_url+"servicios/get_planta_all/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$('#planta-empleado').append('<option value="-1">Seleccionar Planta</option>');
            $.each(data.aaData, function (key,value) {
                $('#planta-empleado').append('<option value="'+ value.idplanta+ '">'+value.nombre+'</option>');
            });
            cargar_combo_area($("#planta-empleado").val());
        }
    });
}

function cargar_combo_area(idplanta){
	$('#area-empleado').text("");
	$('#area-empleado').append('<option value="-1">Seleccionar Area</option>');
	if(idplanta!=-1){
		$.ajax({
			type: "POST",
			url: base_url+"servicios/get_area_all_byIdPlanta/",
			data: {idplanta:idplanta},
			dataType: "json",
			success: function (data) {
			    $.each(data.aaData, function (key,value) {
			        $('#area-empleado').append('<option value="'+ value.idarea+ '">'+value.nombre+'</option>');
			    });
			    if(id_planta_area!=0){
			    	$("#area-empleado").val(id_planta_area);
			    }
			}
		});
	}

}

function cargar_combo_cargo(){
	 $('#cargo-empleado').text("");
	 $.ajax({
        type: "GET",
        url: base_url+"servicios/get_cargo_all/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$('#cargo-empleado').append('<option value="-1">Seleccionar Cargo</option>');
            $.each(data.aaData, function (key,value) {
                $('#cargo-empleado').append('<option value="'+ value.idcargo+ '">'+value.nombre+'</option>');
            });
        }
    });
}

function obtenerkeyCode(){
	 $.ajax({
        type: "GET",
        url: base_url+"empleado/obtenerkeyCode/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$("#keycode-empleado").val(data.aaData.keycode_id);
        }
    });
}